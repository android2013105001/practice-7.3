package resolution.example6.zzeulki.practice73_2;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;

public class ListAct extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        loadDB();
    }

    @Override
    public void onResume(){
        super.onResume();
        loadDB();
    }

    public void loadDB()
    {
        SQLiteDatabase db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );
        db.execSQL("create table if not exists people " + "(_id integer primary key autoincrement, name text, age integer);");

        Cursor c = db.rawQuery("select * from people;",null);
        startManagingCursor(c);

        //Log.i("sk", c.getString(0));

        ListAdapter adapt = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_2,
                c,
                new String[]{"name","age"},
                new int[] {android.R.id.text1,android.R.id.text2},0
        );

        setListAdapter(adapt);

        if(db!=null){
            db.close();
        }


    }
}
