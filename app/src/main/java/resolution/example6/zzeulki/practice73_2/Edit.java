package resolution.example6.zzeulki.practice73_2;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Edit extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
    }

    public void onClick(View v) {
        EditText t = null;
        t = (EditText) findViewById(R.id.editText1);
        String sql = t.getText().toString();

        //String sql = "delete from people where name == '"+name+"';";
        SQLiteDatabase db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );

        db.execSQL(sql);

        finish();
    }
}
