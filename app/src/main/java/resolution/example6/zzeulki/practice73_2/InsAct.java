package resolution.example6.zzeulki.practice73_2;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class InsAct extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ins);
    }

    public void onClick(View v) {
        EditText t = null;
        t = (EditText) findViewById(R.id.editText1);
        String name = t.getText().toString();
        t = (EditText) findViewById(R.id.editText2);
        String age = t.getText().toString();

        String sql = "INSERT INTO people (name,age) values ('" + name + "'," + age + ");";
        SQLiteDatabase db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );

        db.execSQL(sql);

        finish();
    }


}

class PeopleDBHelper extends SQLiteOpenHelper {
    public PeopleDBHelper(Context context){
        super(context,"test.db",null,1);
    }

    public void onCreate(SQLiteDatabase db){
        db.execSQL("create table if not exists people" + " (_id integer primary key autoincrement, name text, age integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table if exists people;");
        onCreate(db);

    }
}
